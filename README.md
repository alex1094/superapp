##### Приложение дипломного проекта и CI/CD

В данном репозитории находится приложение основанное на стандартном образе Nginx, отдающее статичный контент при обращении ([index.html](https://gitlab.com/alex1094/superapp/-/blob/main/index.html)). 

Приложение собирается автоматически в Docker-образ([dockerfile](https://gitlab.com/alex1094/superapp/-/blob/main/dockerfile)) и передается во внешнее хранилище в YandexCloud.

Также в репозитории присутсвует сборка Gitlab CI/CD ([gitlab-ci.yml](https://gitlab.com/alex1094/superapp/-/blob/main/.gitlab-ci.yml)) и состоит из 2 этапов:

1. Этап `build` - собирает (build) docker-образ из Dockerfile с нашим приложением при любом коммите в репозиторий или при добавлении tag. Далее загружает(push) данный образ в репозиторий cr.yandex/crptsn6m55qajbkecilc/app выставляя нужный tag (на основе ветки с которой идет коммент или же тега).
2. этап `deploy` - при добавлении tag, осуществляется дальнейший deploy нашего приложения с помощью `Helm`([helm-chart](https://gitlab.com/alex1094/superapp/-/tree/main/helmDeploy/superapp)) в наш кластер используя созданный ранее образ приложения из yc Container Registry

Детальное описание работы приложения, построения и организации CI/CD, а также основного проекта находятся по ссылке: [Основная часть проекта и его описание](https://github.com/AlexDies/DipIomInfrastructure)
